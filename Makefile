
%.lex.sml: %.lex
	@mllex $<

%.grm.sml: %.grm
	@mlyacc $<

all: tc 

PHONY: all clean test

clean:
	@echo "Cleaning the intermediate files.."
	@rm -f *.lex.sml
	@rm -f *.grm.sml *.grm.desc *.grm.sig
	@rm -f tigc
	@echo "Done!"

test: all
	@echo "Validating on test cases"
	@${CURDIR}/tigc test.txt


tc: ast.sml tiger.mlb tiger.grm.sml indent.sml tiger.lex.sml tiger.sml
	@mlton -output tc tiger.mlb
