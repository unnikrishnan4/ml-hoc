functor TigerLrValsFun(structure Token : TOKEN)
 : sig structure ParserData : PARSER_DATA
       structure Tokens : Tiger_TOKENS
   end
 = 
struct
structure ParserData=
struct
structure Header = 
struct
(*#line 1.2 "tiger.grm"*)(*The grammar is written using online variant of tiger language and the link is

https://www.lrde.epita.fr/~tiger/tiger.html *)

structure A = Ast


(*#line 18.1 "tiger.grm.sml"*)
end
structure LrTable = Token.LrTable
structure Token = Token
local open LrTable in 
val table=let val actionRows =
"\
\\001\000\001\000\000\000\000\000\
\\001\000\001\000\034\000\003\000\034\000\004\000\034\000\005\000\034\000\
\\006\000\012\000\007\000\011\000\008\000\010\000\009\000\009\000\
\\012\000\034\000\013\000\034\000\014\000\034\000\000\000\
\\001\000\001\000\035\000\003\000\035\000\004\000\035\000\005\000\035\000\
\\006\000\012\000\007\000\011\000\008\000\010\000\009\000\009\000\
\\012\000\035\000\013\000\035\000\014\000\035\000\000\000\
\\001\000\003\000\006\000\004\000\005\000\012\000\004\000\000\000\
\\001\000\005\000\023\000\000\000\
\\001\000\006\000\012\000\007\000\011\000\008\000\010\000\009\000\009\000\
\\010\000\008\000\011\000\007\000\013\000\022\000\000\000\
\\029\000\006\000\012\000\007\000\011\000\008\000\010\000\009\000\009\000\
\\010\000\008\000\011\000\007\000\000\000\
\\030\000\003\000\006\000\004\000\005\000\006\000\012\000\007\000\011\000\
\\008\000\010\000\009\000\009\000\010\000\008\000\011\000\007\000\
\\012\000\004\000\000\000\
\\030\000\003\000\006\000\004\000\005\000\012\000\004\000\000\000\
\\031\000\000\000\
\\032\000\006\000\012\000\007\000\011\000\008\000\010\000\009\000\009\000\
\\010\000\008\000\011\000\007\000\014\000\026\000\000\000\
\\033\000\006\000\012\000\007\000\011\000\008\000\010\000\009\000\009\000\
\\010\000\008\000\011\000\007\000\000\000\
\\036\000\000\000\
\\037\000\000\000\
\\038\000\008\000\010\000\009\000\009\000\000\000\
\\039\000\008\000\010\000\009\000\009\000\000\000\
\\040\000\000\000\
\\041\000\000\000\
\"
val actionRowNumbers =
"\003\000\006\000\003\000\008\000\
\\017\000\003\000\003\000\003\000\
\\003\000\003\000\003\000\005\000\
\\004\000\007\000\002\000\001\000\
\\013\000\012\000\015\000\014\000\
\\003\000\016\000\009\000\010\000\
\\003\000\011\000\000\000"
val gotoT =
"\
\\001\000\001\000\003\000\026\000\000\000\
\\000\000\
\\001\000\011\000\000\000\
\\001\000\013\000\002\000\012\000\000\000\
\\000\000\
\\001\000\014\000\000\000\
\\001\000\015\000\000\000\
\\001\000\016\000\000\000\
\\001\000\017\000\000\000\
\\001\000\018\000\000\000\
\\001\000\019\000\000\000\
\\000\000\
\\000\000\
\\001\000\013\000\002\000\022\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\001\000\023\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\001\000\025\000\000\000\
\\000\000\
\\000\000\
\"
val numstates = 27
val numrules = 13
val s = ref "" and index = ref 0
val string_to_int = fn () => 
let val i = !index
in index := i+2; Char.ord(String.sub(!s,i)) + Char.ord(String.sub(!s,i+1)) * 256
end
val string_to_list = fn s' =>
    let val len = String.size s'
        fun f () =
           if !index < len then string_to_int() :: f()
           else nil
   in index := 0; s := s'; f ()
   end
val string_to_pairlist = fn (conv_key,conv_entry) =>
     let fun f () =
         case string_to_int()
         of 0 => EMPTY
          | n => PAIR(conv_key (n-1),conv_entry (string_to_int()),f())
     in f
     end
val string_to_pairlist_default = fn (conv_key,conv_entry) =>
    let val conv_row = string_to_pairlist(conv_key,conv_entry)
    in fn () =>
       let val default = conv_entry(string_to_int())
           val row = conv_row()
       in (row,default)
       end
   end
val string_to_table = fn (convert_row,s') =>
    let val len = String.size s'
        fun f ()=
           if !index < len then convert_row() :: f()
           else nil
     in (s := s'; index := 0; f ())
     end
local
  val memo = Array.array(numstates+numrules,ERROR)
  val _ =let fun g i=(Array.update(memo,i,REDUCE(i-numstates)); g(i+1))
       fun f i =
            if i=numstates then g i
            else (Array.update(memo,i,SHIFT (STATE i)); f (i+1))
          in f 0 handle Subscript => ()
          end
in
val entry_to_action = fn 0 => ACCEPT | 1 => ERROR | j => Array.sub(memo,(j-2))
end
val gotoT=Array.fromList(string_to_table(string_to_pairlist(NT,STATE),gotoT))
val actionRows=string_to_table(string_to_pairlist_default(T,entry_to_action),actionRows)
val actionRowNumbers = string_to_list actionRowNumbers
val actionT = let val actionRowLookUp=
let val a=Array.fromList(actionRows) in fn i=>Array.sub(a,i) end
in Array.fromList(map actionRowLookUp actionRowNumbers)
end
in LrTable.mkLrTable {actions=actionT,gotos=gotoT,numRules=numrules,
numStates=numstates,initialState=STATE 0}
end
end
local open Header in
type pos = int
type arg = unit
structure MlyValue = 
struct
datatype svalue = VOID | ntVOID of unit | CONST of  (int) | ID of  (string) | program of  (A.Program) | exps of  (A.exp list) | exp of  (A.exp)
end
type svalue = MlyValue.svalue
type result = A.Program
end
structure EC=
struct
open LrTable
infix 5 $$
fun x $$ y = y::x
val is_keyword =
fn (T 11) => true | (T 12) => true | (T 13) => true | _ => false
val preferred_change : (term list * term list) list = 
(nil
,nil
 $$ (T 12))::
(nil
,nil
 $$ (T 13))::
nil
val noShift = 
fn (T 0) => true | _ => false
val showTerminal =
fn (T 0) => "EOF"
  | (T 1) => "ID"
  | (T 2) => "CONST"
  | (T 3) => "LPAREN"
  | (T 4) => "RPAREN"
  | (T 5) => "PLUS"
  | (T 6) => "MINUS"
  | (T 7) => "MUL"
  | (T 8) => "DIVIDE"
  | (T 9) => "LT"
  | (T 10) => "GT"
  | (T 11) => "IF"
  | (T 12) => "THEN"
  | (T 13) => "ELSE"
  | _ => "bogus-term"
local open Header in
val errtermvalue=
fn _ => MlyValue.VOID
end
val terms : term list = nil
 $$ (T 13) $$ (T 12) $$ (T 11) $$ (T 10) $$ (T 9) $$ (T 8) $$ (T 7) $$ (T 6) $$ (T 5) $$ (T 4) $$ (T 3) $$ (T 0)end
structure Actions =
struct 
exception mlyAction of int
local open Header in
val actions = 
fn (i392,defaultPos,stack,
    (()):arg) =>
case (i392,stack)
of  ( 0, ( ( _, ( MlyValue.exp exp, exp1left, exp1right)) :: rest671)) => let val  result = MlyValue.program ((*#line 53.31 "tiger.grm"*) Ast.Exptype exp (*#line 207.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 2, ( result, exp1left, exp1right), rest671)
end
|  ( 1, ( rest671)) => let val  result = MlyValue.exps ((*#line 55.34 "tiger.grm"*) []                  (*#line 211.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, defaultPos, defaultPos), rest671)
end
|  ( 2, ( ( _, ( MlyValue.exps exps, _, exps1right)) :: ( _, ( MlyValue.exp exp, exp1left, _)) :: rest671)) => let val  result = MlyValue.exps ((*#line 56.23 "tiger.grm"*)exp :: exps 	   (*#line 215.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, exp1left, exps1right), rest671)
end
|  ( 3, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( MlyValue.exp exp1, _, _)) :: ( _, ( _, IF1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 58.34 "tiger.grm"*)Ast.IfExp ({test= exp1, then'= exp2, else'= NONE})(*#line 219.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 0, ( result, IF1left, exp2right), rest671)
end
|  ( 4, ( ( _, ( MlyValue.exp exp3, _, exp3right)) :: _ :: ( _, ( MlyValue.exp exp2, _, _)) :: _ :: ( _, ( MlyValue.exp exp1, _, _)) :: ( _, ( _, IF1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 59.31 "tiger.grm"*)Ast.IfExp ({test= exp1, then'= exp2, else'= SOME exp3})(*#line 223.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 0, ( result, IF1left, exp3right), rest671)
end
|  ( 5, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 60.26 "tiger.grm"*)A.OpExp({left=exp1, oper=A.Less, right=exp2})(*#line 227.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 0, ( result, exp1left, exp2right), rest671)
end
|  ( 6, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 61.26 "tiger.grm"*)A.OpExp({left=exp1, oper=A.Greater, right=exp2})(*#line 231.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 0, ( result, exp1left, exp2right), rest671)
end
|  ( 7, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 62.26 "tiger.grm"*)A.OpExp({left=exp1, oper=A.Mul, right=exp2})(*#line 235.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 0, ( result, exp1left, exp2right), rest671)
end
|  ( 8, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 63.33 "tiger.grm"*)A.OpExp({left=exp1, oper=A.Div, right=exp2})(*#line 239.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 0, ( result, exp1left, exp2right), rest671)
end
|  ( 9, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 64.30 "tiger.grm"*)A.OpExp({left=exp1, oper=A.Plus, right=exp2})(*#line 243.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 0, ( result, exp1left, exp2right), rest671)
end
|  ( 10, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 65.26 "tiger.grm"*)A.OpExp({left=exp1, oper=A.Minus, right=exp2})(*#line 247.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 0, ( result, exp1left, exp2right), rest671)
end
|  ( 11, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( MlyValue.exps exps, _, _)) :: ( _, ( _, LPAREN1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 66.39 "tiger.grm"*) A.ExpList exps (*#line 251.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 0, ( result, LPAREN1left, RPAREN1right), rest671)
end
|  ( 12, ( ( _, ( MlyValue.CONST CONST, CONST1left, CONST1right)) :: rest671)) => let val  result = MlyValue.exp ((*#line 67.26 "tiger.grm"*) A.Const CONST(*#line 255.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 0, ( result, CONST1left, CONST1right), rest671)
end
| _ => raise (mlyAction i392)
end
val void = MlyValue.VOID
val extract = fn a => (fn MlyValue.program x => x
| _ => let exception ParseInternal
	in raise ParseInternal end) a 
end
end
structure Tokens : Tiger_TOKENS =
struct
type svalue = ParserData.svalue
type ('a,'b) token = ('a,'b) Token.token
fun EOF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 0,(ParserData.MlyValue.VOID,p1,p2))
fun ID (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 1,(ParserData.MlyValue.ID i,p1,p2))
fun CONST (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 2,(ParserData.MlyValue.CONST i,p1,p2))
fun LPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 3,(ParserData.MlyValue.VOID,p1,p2))
fun RPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 4,(ParserData.MlyValue.VOID,p1,p2))
fun PLUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 5,(ParserData.MlyValue.VOID,p1,p2))
fun MINUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 6,(ParserData.MlyValue.VOID,p1,p2))
fun MUL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 7,(ParserData.MlyValue.VOID,p1,p2))
fun DIVIDE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 8,(ParserData.MlyValue.VOID,p1,p2))
fun LT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 9,(ParserData.MlyValue.VOID,p1,p2))
fun GT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 10,(ParserData.MlyValue.VOID,p1,p2))
fun IF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 11,(ParserData.MlyValue.VOID,p1,p2))
fun THEN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 12,(ParserData.MlyValue.VOID,p1,p2))
fun ELSE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 13,(ParserData.MlyValue.VOID,p1,p2))
end
end
