structure Ast = struct
datatype BinOp = Plus
				| Minus
				| Mul
				| Div
				| Less
				| Greater
and exp  = Const of int
               | ExpList of exp list 
				| OpExp of {left: exp, oper: BinOp, right: exp}
				| IfExp of {test: exp, then': exp, else': exp option}



fun binOpToString Plus		= "+"
  | binOpToString Minus		= "-"
  | binOpToString Mul		= "*"
  | binOpToString Div		= "/"
  | binOpToString Less		= ">"
  | binOpToString Greater	= "<"
datatype Program = Exptype of exp 
end
