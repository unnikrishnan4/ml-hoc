val green = "\027[1;36m"
val white = "\027[0m"
val grey = "\027[0;37m"
val red = "\027[0;31m"
val brown = "\027[0;33m"
val blue = "\027[0;34m"
structure Indentation =
struct
  fun  put s = if (s = 0) then ("") else ("  "^(put (s-1)))
  fun indent s (Ast.Const x) = (Int.toString x)
    |indent s (Ast.OpExp({left=a, oper=oper, right=b})) = "("^( indent 0 a)^(Ast.binOpToString oper)^( indent 0 b)^")"
    |indent s (Ast.IfExp ({test=a, then'=b, else'=c}) ) =
		(case c of NONE => (put s)^red^"if"^white^" " ^ (indent s a)^"\n"^(put s)^red^" then "^white^(indent (s+1)b) 
			|SOME e =>  (indent s (Ast.IfExp({test=a, then'=b,else'=NONE})))^(put s)^red^"else "^white^(indent (s+1) e))
   				
and

  pretty s (Ast.Exptype a)  = (indent s a)^"\n"

     
end
